
		
			<!-- /Widgets Footer -->			
			<!-- Contacts Footer  -->
            <footer class="contacts_wrap bg_tint_dark contacts_style_dark">
                <div class="content_wrap">
                    <div class="logo">
                        <a href="index.html">
							<img src="images/logo_footer.png" alt="">
						</a>
                    </div>
                    <div class="contacts_address">
                        <address class="address_right">
							Phone: 1.800.123.4567<br>
							Fax: 1.800.123.4566
						</address>
                        <address class="address_left">
							San Francisco, CA 94102, US<br>	
							1234 Some St
						</address>
                    </div>
                    <div class="sc_socials sc_socials_size_big">
                        <div class="sc_socials_item">
							<a href="#" target="_blank" class="social_icons social_facebook">
								<span class="sc_socials_hover social_facebook"></span>
							</a>
						</div>
                        <div class="sc_socials_item">
							<a href="#" target="_blank" class="social_icons social_pinterest">
								<span class="sc_socials_hover social_pinterest"></span>
							</a>
						</div>
                        <div class="sc_socials_item">
							<a href="#" target="_blank" class="social_icons social_twitter">
								<span class="sc_socials_hover social_twitter"></span>
							</a>
						</div>
                        <div class="sc_socials_item">
							<a href="#" target="_blank" class="social_icons social_gplus">
								<span class="sc_socials_hover social_gplus"></span>
							</a>
						</div>
                        <div class="sc_socials_item">
							<a href="#" target="_blank" class="social_icons social_rss">
								<span class="sc_socials_hover social_rss"></span>
							</a>
						</div>
                        <div class="sc_socials_item">
							<a href="#" target="_blank" class="social_icons social_dribbble">
								<span class="sc_socials_hover social_dribbble"></span>
							</a>
						</div>
                    </div>
                </div>
            </footer>
            <!-- /Contacts Footer -->
			<!-- Copyright -->
            <div class="copyright_wrap">
                <div class="content_wrap">
                    <p>© 2015 All Rights Reserved. <a href="#">Terms of use</a> and <a href="#">Privacy Policy</a></p>
                </div>
            </div>
			<!-- /Copyright -->
        </div>
    </div>
    <!-- /Body -->
    <a href="#" class="scroll_to_top icon-up-2" title="Scroll to top"></a>

    <div class="custom_html_section"></div>
		
	<script type="text/javascript" src="js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="js/jquery/ui/core.min.js"></script>
    <script type="text/javascript" src="js/jquery/ui/widget.min.js"></script>
    <script type="text/javascript" src="js/jquery/ui/tabs.min.js"></script>
    <script type="text/javascript" src="js/jquery/ui/effect.min.js"></script>
    <script type="text/javascript" src="js/jquery/ui/effect-fade.min.js"></script>	
	<script type="text/javascript" src="js/jquery/jquery.blockUI.min.js"></script>
    <script type="text/javascript" src="js/jquery/jquery.cookie.min.js"></script>	
	
	<script type="text/javascript" src="js/global.min.js"></script>
    <script type="text/javascript" src="js/core.utils.min.js"></script>
    <script type="text/javascript" src="js/core.init.min.js"></script>   
    <script type="text/javascript" src="js/shortcodes/shortcodes.min.js"></script>

    <script type="text/javascript" src="js/superfish.min.js"></script>
    <script type="text/javascript" src="js/jquery.slidemenu.min.js"></script>

    <script type="text/javascript" src="js/mediaelement/mediaelement-and-player.min.js"></script>
    <script type="text/javascript" src="js/mediaelement/wp-mediaelement.min.js"></script>

    <script type="text/javascript" src="js/core.messages/core.messages.min.js"></script>

	<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="js/hover/jquery.hoverdir.min.js"></script>
	<script type="text/javascript" src="js/prettyPhoto/jquery.prettyPhoto.min.js"></script>		
    <script type="text/javascript" src="js/swiper/idangerous.swiper-2.7.min.js"></script>
    <script type="text/javascript" src="js/swiper/idangerous.swiper.scrollbar-2.4.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="js/core.googlemap.min.js"></script>
    <script type="text/javascript" src="js/diagram/chart.min.js"></script>
    <script type="text/javascript" src="js/core.customizer/front.customizer.min.js"></script>
	<script type="text/javascript" src="js/skin.customizer.min.js"></script>	

</body>


<!-- Mirrored from education-html.themerex.net/courses-streampage.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 05:53:16 GMT -->
</html>