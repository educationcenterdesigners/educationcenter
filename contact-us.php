
<?php include 'header.php';?>
			<!-- Page title -->
            <div class="page_top_wrap page_top_title page_top_breadcrumbs sc_pt_st1">
                <div class="content_wrap">
                    <div class="breadcrumbs">
                        <a class="breadcrumbs_item home" href="index.html">Home</a>
						<span class="breadcrumbs_delimiter"></span>
						<span class="breadcrumbs_item current">Contact Us</span>
					</div>
                    <h1 class="page_title">Contact Us</h1>
                </div>
            </div>
			<!-- /Page title -->
			<!-- Content without sidebar -->
            <div class="page_content_wrap">
                <div class="content">
					<article class="post_item post_item_single page">						
                        <section class="post_content">
							<!-- Features section -->
							<div class="sc_section">
								<div class="sc_content content_wrap margin_top_3em_imp margin_bottom_3em_imp">
									<div class="columns_wrap sc_columns columns_fluid sc_columns_count_3">
										<div class="column-1_3 sc_column_item sc_column_item_1 odd first text_center">
											<span class="sc_icon icon-mail-2 link_color font_5em lh_1em"></span>
											<div class="sc_section margin_top_1em_imp">
												<p><a href="#"><span class="__cf_email__" data-cfemail="d1b8bfb7be91a8bea4a3a2b8a5b4ffb2bebc">[email&#160;protected]</span><script data-cfhash='f9e31' type="text/javascript">
/* <![CDATA[ */!function(){try{var t="currentScript"in document?document.currentScript:function(){for(var t=document.getElementsByTagName("script"),e=t.length;e--;)if(t[e].getAttribute("data-cfhash"))return t[e]}();if(t&&t.previousSibling){var e,r,n,i,c=t.previousSibling,a=c.getAttribute("data-cfemail");if(a){for(e="",r=parseInt(a.substr(0,2),16),n=2;a.length-n;n+=2)i=parseInt(a.substr(n,2),16)^r,e+=String.fromCharCode(i);e=document.createTextNode(e),c.parentNode.replaceChild(e,c)}t.parentNode.removeChild(t);}}catch(u){}}()/* ]]> */</script></a><br />
													<a href="#">www.yoursite.com</a>
												</p>
											</div>
										</div>
										<div class="column-1_3 sc_column_item sc_column_item_2 even text_center">
											<span class="sc_icon icon-telephone-2 link_color font_5em lh_1em"></span>
											<div class="sc_section margin_top_1em_imp sc_features_st1">
												<p>+1(800)123-4567
													<br /> +1(800)123-4566
												</p>
											</div>
										</div>
										<div class="column-1_3 sc_column_item sc_column_item_3 odd text_center">
											<span class="sc_icon icon-map-2 link_color font_5em lh_1em"></span>
											<div class="sc_section margin_top_1em_imp sc_features_st1">
												<p>176 W street name,<br /> 
													New York, NY 10014
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Features section -->
							<!-- Contact form -->
							<div class="log-in_img">
								<div class="sc_section_overlay sc_contact_bg_color" data-overlay="0.8" data-bg_color="#024b5e">
									<div class="sc_section_content">
										<div class="sc_content content_wrap margin_top_3em_imp margin_bottom_3_5em_imp">
											<div id="sc_contact_form" class="sc_contact_form sc_contact_form_standard aligncenter width_80per">
												<h2 class="sc_contact_form_title">Contact Us Today</h2>
												<p class="sc_contact_form_description">Your email address will not be published. Required fields are marked *</p>
												<form id="sc_contact_form_1" data-formtype="contact" method="post" action="#">
													<div class="sc_contact_form_info">
														<div class="sc_contact_form_item sc_contact_form_field label_over">
															<label class="required" for="sc_contact_form_username">Name</label>
															<input id="sc_contact_form_username" type="text" name="username" placeholder="Name *">
														</div>
														<div class="sc_contact_form_item sc_contact_form_field label_over">
															<label class="required" for="sc_contact_form_email">E-mail</label>
															<input id="sc_contact_form_email" type="text" name="email" placeholder="E-mail *">
														</div>
														<div class="sc_contact_form_item sc_contact_form_field label_over">
															<label class="required" for="sc_contact_form_subj">Subject</label>
															<input id="sc_contact_form_subj" type="text" name="subject" placeholder="Subject">
														</div>
													</div>
													<div class="sc_contact_form_item sc_contact_form_message label_over">
														<label class="required" for="sc_contact_form_message">Message</label>
														<textarea id="sc_contact_form_message" name="message" placeholder="Message"></textarea>
													</div>
													<div class="sc_contact_form_item sc_contact_form_button">
														<button>SEND MESSAGE</button>
													</div>
													<div class="result sc_infobox"></div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Contact form -->
                        </section>
                    </article>
                </div>
            </div>
            <!-- /Content without sidebar -->
			<!-- Google map -->
			<div id="sc_googlemap_1213982173" class="sc_googlemap  width_100per height_30em" data-title="" data-description="San Francisco, CA 94102, US" data-address="San Francisco, CA 94102, US" data-latlng="" data-zoom="16" data-style="default" data-point="">
			</div>
			<!-- /Google map -->
			<!-- Contacts Footer  -->
           
<?php include 'footer.php';?>