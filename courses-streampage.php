
<?php include 'header.php';?>
			<!-- Page title -->
            <div class="page_top_wrap page_top_title page_top_breadcrumbs sc_pt_st1">
                <div class="content_wrap">
                    <div class="breadcrumbs">
                        <a class="breadcrumbs_item home" href="index.html">Home</a>
						<span class="breadcrumbs_delimiter"></span>
						<span class="breadcrumbs_item current">All courses</span> 
					</div>
                    <h1 class="page_title">All courses</h1>
                </div>
            </div>
			<!-- /Page title -->
			<!-- Content with sidebar -->
            <div class="page_content_wrap">
                <div class="content_wrap">
                    <div class="content">
                        <div class="isotope_filters isotope-courses-streampage"></div>
                        <div class="isotope_wrap" data-columns="3">
							<!-- Courses item -->
                            <div class="isotope_item isotope_item_courses isotope_column_3 flt_55">
                                <article class="post_item post_item_courses odd">
                                    <div class="post_content isotope_item_content ih-item colored square effect_dir left_to_right">									
                                        <div class="post_featured img">
                                            <a href="paid-course.html">
												<img alt="Principles of Written English, Part 2" src="images/masonry_15-400x400.jpg"></a>
                                            <h4 class="post_title">
												<a href="paid-course.html">Principles of Written English, Part 2</a>
											</h4>
                                            <div class="post_descr">
                                                <div class="post_price">
													<span class="post_price_value">$85</span>
													<span class="post_price_period">monthly</span>
												</div>
                                                <div class="post_category">
													<a href="tag-page.html">Language</a>
												</div>
                                                <div class="post_rating reviews_summary blog_reviews">
                                                    <div class="criteria_summary criteria_row">
                                                        <div class="reviews_stars reviews_style_stars" data-mark="60.5">
                                                            <div class="reviews_stars_wrap">
                                                                <div class="reviews_stars_bg"><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span></div>
                                                                <div class="reviews_stars_hover width_61per"><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span></div>
                                                            </div>
                                                            <div class="reviews_value">60.5</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="post_info_wrap info">
                                            <div class="info-back">
                                                <h4 class="post_title">
													<a href="paid-course.html">Principles of Written English, Part 2</a>
												</h4>
                                                <div class="post_descr">
                                                    <p>
														<a href="paid-course.html">Nam id leo massa. Cras at condimentum nisi, vulputate ultrices turpis.</a>
													</p>
                                                    <div class="post_buttons">
                                                        <div class="post_button">
															<a href="paid-course.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">LEARN MORE</a>
														</div>
                                                        <div class="post_button">
															<a href="product-page.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">BUY NOW</a>
														</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <!-- /Courses item -->
							<!-- Courses item -->
                            <div class="isotope_item isotope_item_courses isotope_column_3 flt_43">
                                <article class="post_item post_item_courses even">
                                    <div class="post_content isotope_item_content ih-item colored square effect_dir left_to_right">
                                        <div class="post_featured img">
                                            <a href="#">
												<img alt="Entrepreneurship 101: Who is your customer?" src="images/masonry_06-400x400.jpg">
											</a>
                                            <h4 class="post_title">
												<a href="paid-course.html">Entrepreneurship 101:  Who is your customer?</a>
											</h4>
                                            <div class="post_descr">
                                                <div class="post_price">
													<span class="post_price_value">$195</span>
													<span class="post_price_period">monthly</span>
												</div>
                                                <div class="post_category">
													<a href="tag-page.html">Marketing</a>
												</div>
                                                <div class="post_rating reviews_summary blog_reviews">
                                                    <div class="criteria_summary criteria_row">
                                                        <div class="reviews_stars reviews_style_stars" data-mark="76.3">
                                                            <div class="reviews_stars_wrap">
                                                                <div class="reviews_stars_bg"><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span></div>
                                                                <div class="reviews_stars_hover width_76per"><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span></div>
                                                            </div>
                                                            <div class="reviews_value">76.3</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="post_info_wrap info">
                                            <div class="info-back">
                                                <h4 class="post_title">
													<a href="paid-course.html">Entrepreneurship 101:  Who is your customer?</a>
												</h4>
                                                <div class="post_descr">
                                                    <p>
														<a href="paid-course.html">Quisque a nulla eget ante vestibulum lacinia eu quis massa.</a>
													</p>
                                                    <div class="post_buttons">
                                                        <div class="post_button">
															<a href="paid-course.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">LEARN MORE</a>
														</div>
                                                        <div class="post_button">
															<a href="product-page.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">BUY NOW</a>
														</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <!-- /Courses item -->
							<!-- Courses item -->
                            <div class="isotope_item isotope_item_courses isotope_column_3 flt_57">
                                <article class="post_item post_item_courses odd">
                                    <div class="post_content isotope_item_content ih-item colored square effect_dir left_to_right">
                                        <div class="post_featured img">
                                            <a href="free-course.html">
												<img alt="Evaluating Social Programs" src="images/masonry_04-400x400.jpg">
											</a>
                                            <h4 class="post_title">
												<a href="free-course.html">Evaluating Social Programs</a>
											</h4>
                                            <div class="post_descr">
                                                <div class="post_price">
													<span class="post_price_value">Free!</span>
												</div>
                                                <div class="post_category">
													<a href="tag-page.html">Social</a>
												</div>
                                                <div class="post_rating reviews_summary blog_reviews">
                                                    <div class="criteria_summary criteria_row">
                                                        <div class="reviews_stars reviews_style_stars" data-mark="53.5">
                                                            <div class="reviews_stars_wrap">
                                                                <div class="reviews_stars_bg"><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span></div>
                                                                <div class="reviews_stars_hover width_54per" ><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span></div>
                                                            </div>
                                                            <div class="reviews_value">53.5</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="post_info_wrap info">
                                            <div class="info-back">
                                                <h4 class="post_title">
													<a href="free-course.html">Evaluating Social  Programs</a>
												</h4>
                                                <div class="post_descr">
                                                    <p>
														<a href="free-course.html">Nunc finibus vestibulum dui a fringilla. Maecenas maximus in massa sit amet maximus.</a>
													</p>
                                                    <div class="post_buttons">
                                                        <div class="post_button">
															<a href="free-course.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">LEARN MORE</a>
														</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <!-- /Courses item -->
							<!-- Courses item -->
                            <div class="isotope_item isotope_item_courses isotope_column_3 flt_55">
                                <article class="post_item post_item_courses even">
                                    <div class="post_content isotope_item_content ih-item colored square effect_dir left_to_right">
                                        <div class="post_featured img">
                                            <a href="paid-course.html">
												<img alt="Principles of Written English, Part 1" src="images/masonry_05-400x400.jpg">
											</a>
                                            <h4 class="post_title">
												<a href="paid-course.html">Principles of Written English, Part 1</a>
											</h4>
                                            <div class="post_descr">
                                                <div class="post_price">
													<span class="post_price_value">$85</span>
												</div>
                                                <div class="post_category">
													<a href="tag-page.html">Language</a>
												</div>
                                                <div class="post_rating reviews_summary blog_reviews">
                                                    <div class="criteria_summary criteria_row">
                                                        <div class="reviews_stars reviews_style_stars" data-mark="86.8">
                                                            <div class="reviews_stars_wrap">
                                                                <div class="reviews_stars_bg"><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span></div>
                                                                <div class="reviews_stars_hover width_87per"><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span></div>
                                                            </div>
                                                            <div class="reviews_value">86.8</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="post_info_wrap info">
                                            <div class="info-back">
                                                <h4 class="post_title">
													<a href="paid-course.html">Principles of Written English, Part 1</a>
												</h4>
                                                <div class="post_descr">
                                                    <p>
														<a href="paid-course.html">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</a>
													</p>
                                                    <div class="post_buttons">
                                                        <div class="post_button">
															<a href="paid-course.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">LEARN MORE</a>
														</div>
                                                        <div class="post_button">
															<a href="product-page.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">BUY NOW</a>
														</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <!-- /Courses item -->
							<!-- Courses item -->
                            <div class="isotope_item isotope_item_courses isotope_column_3 flt_52">
                                <article class="post_item post_item_courses odd">
                                    <div class="post_content isotope_item_content ih-item colored square effect_dir left_to_right">
                                        <div class="post_featured img">
                                            <a href="paid-course.html">
												<img alt="Introduction to Biomedical Imaging" src="images/masonry_03-400x400.jpg"></a>
                                            <h4 class="post_title">
												<a href="paid-course.html">Introduction to Biomedical Imaging</a>
											</h4>
                                            <div class="post_descr">
                                                <div class="post_price">
													<span class="post_price_value">$400</span>
												</div>
                                                <div class="post_category">
													<a href="tag-page.html">Medicine</a>
												</div>
                                                <div class="post_rating reviews_summary blog_reviews">
                                                    <div class="criteria_summary criteria_row">
                                                        <div class="reviews_stars reviews_style_stars" data-mark="74.8">
                                                            <div class="reviews_stars_wrap">
                                                                <div class="reviews_stars_bg"><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span></div>
                                                                <div class="reviews_stars_hover width_75per"><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span></div>
                                                            </div>
                                                            <div class="reviews_value">74.8</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="post_info_wrap info">
                                            <div class="info-back">
                                                <h4 class="post_title">
													<a href="paid-course.html">Introduction to Biomedical Imaging</a>
												</h4>
                                                <div class="post_descr">
                                                    <p>
														<a href="paid-course.html">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a>
													</p>
                                                    <div class="post_buttons">
                                                        <div class="post_button">
															<a href="paid-course.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">LEARN MORE</a>
														</div>
                                                        <div class="post_button">
															<a href="product-page.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">BUY NOW</a>
														</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <!-- /Courses item -->
							<!-- Courses item -->
                            <div class="isotope_item isotope_item_courses isotope_column_3 flt_53">
                                <article class="post_item post_item_courses even last">
                                    <div class="post_content isotope_item_content ih-item colored square effect_dir left_to_right">
                                        <div class="post_featured img">
                                            <a href="paid-course.html">
												<img alt="Introduction to Computer  Science" src="images/masonry_02-400x400.jpg">
											</a>
                                            <h4 class="post_title">
												<a href="paid-course.html">Introduction to Computer  Science</a>
											</h4>
                                            <div class="post_descr">
                                                <div class="post_price">
													<span class="post_price_value">$120</span>
													<span class="post_price_period">monthly</span>
												</div>
                                                <div class="post_category">
													<a href="tag-page.html">Computers</a>
												</div>
                                                <div class="post_rating reviews_summary blog_reviews">
                                                    <div class="criteria_summary criteria_row">
                                                        <div class="reviews_stars reviews_style_stars" data-mark="73">
                                                            <div class="reviews_stars_wrap">
                                                                <div class="reviews_stars_bg"><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span></div>
                                                                <div class="reviews_stars_hover width_73per"><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span></div>
                                                            </div>
                                                            <div class="reviews_value">73</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="post_info_wrap info">
                                            <div class="info-back">
                                                <h4 class="post_title">
													<a href="paid-course.html">Introduction to Computer  Science</a>
												</h4>
                                                <div class="post_descr">
                                                    <p>
														<a href="paid-course.html">Sed interdum felis diam, vitae rutrum urna laoreet vehicula.</a>
													</p>
                                                    <div class="post_buttons">
                                                        <div class="post_button">
															<a href="paid-course.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">LEARN MORE</a>
														</div>
                                                        <div class="post_button">
															<a href="product-page.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">BUY NOW</a>
														</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
							<!-- /Courses item -->
                        </div>
                        <div id="viewmore" class="pagination_wrap pagination_viewmore">
                            <a href="#" id="viewmore_link" class="theme_button viewmore_button">
								<span class="icon-spin3 animate-spin viewmore_loading"></span>
								<span class="viewmore_text_1">LOAD MORE</span>
								<span class="viewmore_text_2">Loading ...</span>
							</a>
                            <span class="viewmore_loader"></span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Content -->
			<!-- Partners footer -->
            <footer class="user_footer_wrap">
                <div class="sc_section margin_bottom_1_imp sc_footer_custom_bg1">
                    <div class="sc_section_overlay">
                        <div class="sc_section_content">
                            <div class="sc_content content_wrap">
                                <div class="sc_section aligncenter width_70per">
                                    <h2 class="sc_title sc_title_regular margin_top_05em">Schools &amp; Partners</h2> 
									We believe in offering the highest quality courses, created by schools and partners who share our commitment to excellence in teaching and learning, both online and in the classroom.
								</div>
                                <div id="sc_section_2" class="sc_section margin_top_1_5em_imp margin_bottom_075em_imp height_75">
                                    <div id="sc_section_2_scroll" class="sc_scroll sc_scroll_horizontal swiper-slider-container scroll-container height_75">
                                        <div class="sc_scroll_wrapper swiper-wrapper">
                                            <div class="sc_scroll_slide swiper-slide">
                                                <figure class="sc_image alignleft sc_image_shape_square margin_right_0_imp">
													<img src="images/partners_01.jpg" alt="" />
												</figure>
                                                <figure class="sc_image alignleft sc_image_shape_square margin_right_0_imp margin_left_4em_imp">
													<img src="images/partners_02.jpg" alt="" />
												</figure>
                                                <figure class="sc_image alignleft sc_image_shape_square margin_right_0_imp margin_left_4em_imp">
													<img src="images/partners_03.jpg" alt="" />
												</figure>
                                                <figure class="sc_image alignleft sc_image_shape_square margin_right_0_imp margin_left_4em_imp">
													<img src="images/partners_04.jpg" alt="" />
												</figure>
                                                <figure class="sc_image alignleft sc_image_shape_square margin_right_0_imp margin_left_4em_imp">
													<img src="images/partners_05.jpg" alt="" />
												</figure>
                                                <figure class="sc_image alignleft sc_image_shape_square margin_right_0_imp margin_left_4em_imp">
													<img src="images/partners_06.jpg" alt="" />
												</figure>
                                            </div>
                                        </div>
                                        <div id="sc_section_2_scroll_bar" class="sc_scroll_bar sc_scroll_bar_horizontal sc_section_2_scroll_bar"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>	
			<!-- /Partners footer -->
			<?php include 'footer.php';?>