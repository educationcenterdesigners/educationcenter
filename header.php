<!DOCTYPE html>
<html lang="en-US">


<!-- Mirrored from education-html.themerex.net/contact-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 05:53:10 GMT -->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    <title>Contact Us | Education Center</title>

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&amp;subset=latin%2Clatin-ext&amp;ver=4.3.1" type="text/css" media="all" />
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,700,700italic&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" type="text/css" media="all" />
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Love+Ya+Like+A+Sister:400&amp;subset=latin" type="text/css" media="all" />
	<link rel="stylesheet" href="css/fontello/css/fontello.css" type="text/css" media="all" />
	
    <link rel="stylesheet" href="js/rs-plugin/settings.css" type="text/css" media="all" />

    <link rel="stylesheet" href="css/woocommerce/woocommerce-layout.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/woocommerce/woocommerce-smallscreen.css" type="text/css" media="only screen and (max-width: 768px)" />
    <link rel="stylesheet" href="css/woocommerce/woocommerce.css" type="text/css" media="all" />

    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/shortcodes.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/core.animation.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/tribe-style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/skins/skin.css" type="text/css" media="all" />

	<link rel="stylesheet" href="css/core.portfolio.css" type="text/css" media="all" />
    <link rel="stylesheet" href="js/mediaelement/mediaelementplayer.min.css" type="text/css" media="all" />
    <link rel="stylesheet" href="js/mediaelement/wp-mediaelement.css" type="text/css" media="all" />
    <link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="all" />	
    <link rel="stylesheet" href="js/core.customizer/front.customizer.css" type="text/css" media="all" />
	<link rel="stylesheet" href="js/core.messages/core.messages.css" type="text/css" media="all" />
    <link rel="stylesheet" href="js/swiper/idangerous.swiper.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/custom-style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/skins/skin-responsive.css" type="text/css" media="all" />
</head>

<body class="page body_style_fullscreen body_filled article_style_stretch template_single-standard top_panel_style_dark top_panel_opacity_solid top_panel_above menu_right sidebar_hide">
    <a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-angle-double-up" data-url="" data-separator="yes"></a>
	<!-- Body -->
    <div class="body_wrap">
        <div class="page_wrap">
            <div class="top_panel_fixed_wrap"></div>
            <header class="top_panel_wrap bg_tint_dark">
				<!-- User menu -->
                <div class="menu_user_wrap">
                    <div class="content_wrap clearfix">
                        <div class="menu_user_area menu_user_right menu_user_nav_area">
                            <ul id="menu_user" class="menu_user_nav">
                                <li class="menu_user_bookmarks">
                                    <a href="#" class="bookmarks_show icon-star-1" title="Show bookmarks"></a>
                                    <ul class="bookmarks_list">
                                        <li><a href="#" class="bookmarks_add icon-star-empty" title="Add the current page into bookmarks">Add bookmark</a></li>
                                    </ul>
                                </li>
                                <li class="menu_user_controls">
                                    <a href="#">
										<span class="user_avatar">
											<img alt="" src="http://1.gravatar.com/avatar/45e4d63993e55fa97a27d49164bce80f?s=16&amp;d=mm&amp;r=g" srcset="http://1.gravatar.com/avatar/45e4d63993e55fa97a27d49164bce80f?s=32&amp;d=mm&amp;r=g 2x" class="avatar avatar-16 photo" height="16" width="16" />
										</span>
										<span class="user_name">John Doe</span></a>
                                    <ul>
                                        <li><a href="#" class="icon icon-doc-inv">New post</a></li>
                                        <li><a href="#" class="icon icon-cog-1">Settings</a></li>
                                    </ul>
                                </li>
                                <li class="menu_user_logout">
									<a href="#" class="icon icon-logout">Logout</a>
								</li>
                            </ul>
                        </div>
                        <div class="menu_user_area menu_user_left menu_user_contact_area">Contact us on 0 800 123-4567 or <a href="#"><span class="__cf_email__" data-cfemail="89fafcf9f9e6fbfdc9fde1ece4ecfbecf1a7e7ecfd">[email&#160;protected]</span><script data-cfhash='f9e31' type="text/javascript">
/* <![CDATA[ */!function(){try{var t="currentScript"in document?document.currentScript:function(){for(var t=document.getElementsByTagName("script"),e=t.length;e--;)if(t[e].getAttribute("data-cfhash"))return t[e]}();if(t&&t.previousSibling){var e,r,n,i,c=t.previousSibling,a=c.getAttribute("data-cfemail");if(a){for(e="",r=parseInt(a.substr(0,2),16),n=2;a.length-n;n+=2)i=parseInt(a.substr(n,2),16)^r,e+=String.fromCharCode(i);e=document.createTextNode(e),c.parentNode.replaceChild(e,c)}t.parentNode.removeChild(t);}}catch(u){}}()/* ]]> */</script></a></div>
                    </div>
                </div>
				<!-- /User menu -->
				<!-- Main menu -->
                <div class="menu_main_wrap logo_left">					
                    <div class="content_wrap clearfix">
						<!-- Logo -->
                        <div class="logo">
                            <a href="index.html">
								<img src="images/logo_dark.png" class="logo_main" alt="">
								<img src="images/logo_dark.png" class="logo_fixed" alt="">
							</a>
                        </div>
						<!-- Logo -->
						<!-- Search 
                        <div class="search_wrap search_style_regular search_ajax" title="Open/close search form">
                            <a href="#" class="search_icon icon-search-2"></a>
                            <div class="search_form_wrap">
                                <form method="get" class="search_form" action="#">
                                    <button type="submit" class="search_submit icon-zoom-1" title="Start search"></button>
                                    <input type="text" class="search_field" placeholder="" value="" name="s" title="" />
                                </form>
                            </div>
                            <div class="search_results widget_area bg_tint_light">
                                <a class="search_results_close icon-delete-2"></a>
                                <div class="search_results_content">
							</div>
                            </div>
                        </div>
						<!-- /Search -->
						<!-- Navigation -->
                        <a href="#" class="menu_main_responsive_button icon-menu-1"></a>
						<nav class="menu_main_nav_area">
							<ul id="menu_main" class="menu_main_nav">
								<li class="menu-item menu-item-has-children"><a href="index.html">Homepage</a>
									<ul class="sub-menu">
										<li class="menu-item"><a href="index.html">Homepage Wide</a></li>
										<li class="menu-item"><a href="homepage-2.html">Homepage Boxed</a></li>
										<li class="menu-item"><a href="homepage-3.html">Homepage Photos</a></li>
									</ul>
								</li>
								<li class="menu-item current-menu-ancestor current-menu-parent menu-item-has-children"><a href="#">Features</a>
									<ul class="sub-menu">
										<li class="menu-item"><a href="typography.html">Typography</a></li>
										<li class="menu-item"><a href="shortcodes.html">Shortcodes</a></li>
										<li class="menu-item"><a href="video-tutorials.html">Video Tutorials</a></li>
										<li class="menu-item"><a href="events.html">Events Calendar</a></li>
										<li class="menu-item"><a href="about-us.html">About Us</a></li>
										<li class="menu-item current-menu-item page_item current_page_item"><a href="contact-us.html">Contact Us</a></li>
										<li class="menu-item"><a href="not-existing-page.html">Page 404</a></li>
										<li class="menu-item"><a href="not-existing-page-2.html">Page 404 (Style 2)</a></li>
									</ul>
								</li>
								<li class="menu-item menu-item-has-children"><a href="courses-streampage.php">Courses</a>
									<ul class="sub-menu">
										<li class="menu-item"><a href="courses-streampage.html">All courses</a></li>
										<li class="menu-item"><a href="free-course.html">Free course</a></li>
										<li class="menu-item"><a href="paid-course.html">Paid course</a></li>
										<li class="menu-item menu-item-has-children"><a href="#">Lessons</a>
											<ul class="sub-menu">
												<li class="menu-item"><a href="free-lesson.html">Free lesson (started)</a></li>
												<li class="menu-item"><a href="free-lesson-coming-soon.html">Free lesson (coming soon)</a></li>
												<li class="menu-item"><a href="lesson-from-paid-course.html">Lesson from paid course</a></li>
											</ul>
										</li>
									</ul>
								</li>
								<li class="menu-item menu-item-has-children"><a href="team-members.php">Teachers</a>
									<ul class="sub-menu">
										<li class="menu-item"><a href="team-members.html">Teachers Team</a></li>
										<li class="menu-item"><a href="personal-page.html">Teacher&#8217;s Personal Page</a></li>
									</ul>
								</li>
								<li class="menu-item menu-item-has-children"><a href="blog-streampage.php">Blog</a>
									<ul class="sub-menu">
										<li class="menu-item menu-item-has-children"><a href="#">Post Formats</a>
											<ul class="sub-menu">
												<li class="menu-item"><a href="post-formats-with-sidebar.html">With Sidebar</a></li>
												<li class="menu-item"><a href="post-formats.html">Without sidebar</a></li>
											</ul>
										</li>
										<li class="menu-item menu-item-has-children"><a href="#">Masonry tiles</a>
											<ul class="sub-menu">
												<li class="menu-item"><a href="masonry-2-columns.html">Masonry (2 columns)</a></li>
												<li class="menu-item"><a href="masonry-3-columns.html">Masonry (3 columns)</a></li>
											</ul>
										</li>
										<li class="menu-item menu-item-has-children"><a href="#">Portfolio tiles</a>
											<ul class="sub-menu">
												<li class="menu-item"><a href="portfolio-2-columns.html">Portfolio (2 columns)</a></li>
												<li class="menu-item"><a href="portfolio-3-columns.html">Portfolio (3 columns)</a></li>
												<li class="menu-item menu-item-has-children"><a href="#">Portfolio hovers</a>
													<ul class="sub-menu">
														<li class="menu-item"><a href="portfolio-hovers-circle.html">Circle, Part 1</a></li>
														<li class="menu-item"><a href="portfolio-hovers-circle-part-2.html">Circle, Part 2</a></li>
														<li class="menu-item"><a href="portfolio-hovers-circle-part-3.html">Circle, Part 3</a></li>
														<li class="menu-item"><a href="portfolio-hovers-square.html">Square, Part 1</a></li>
														<li class="menu-item"><a href="portfolio-hovers-square-part-2.html">Square, Part 2</a></li>
														<li class="menu-item"><a href="portfolio-hovers-square-part-3.html">Square, Part 3</a></li>
													</ul>
												</li>
											</ul>
										</li>
									</ul>
								</li>
								<li class="menu-item"><a href="products.php">Shop</a></li>
							</ul>
						</nav>
						<!-- /Navigation -->
						<!--search box-->
								<div class="search">
						<div class="subject">
						<form>
							<select required>
								<option value="" disabled selected hidden>Subject</option>
								<option value="0">Science</option>
								<option value="1">History</option>
							</select>
						</form>
						</div>
						<div class="category">
						<form>
							<select required>
								<option value="" disabled selected hidden>Category</option>
								<option value="0">Science</option>
								<option value="1">History</option>
							</select>
						</form>
						</div>
					
						
						
						<div class="submit"><input type="submit" value="Search your Tutor">
						</div>
						
						
						
                    
					</div>
						<!--search box-->
                    </div>
                </div>
				<!-- /Main menu -->
            </header>