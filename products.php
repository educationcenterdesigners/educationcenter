<!DOCTYPE html>
<html lang="en-US">


<!-- Mirrored from education-html.themerex.net/products.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 05:56:15 GMT -->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    <title>Products | Education Center</title>

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&amp;subset=latin%2Clatin-ext&amp;ver=4.3.1" type="text/css" media="all" />
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,700,700italic&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" type="text/css" media="all" />
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Love+Ya+Like+A+Sister:400&amp;subset=latin" type="text/css" media="all" />
	<link rel="stylesheet" href="css/fontello/css/fontello.css" type="text/css" media="all" />
	
    <link rel="stylesheet" href="js/rs-plugin/settings.css" type="text/css" media="all" />

    <link rel="stylesheet" href="css/woocommerce/woocommerce-layout.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/woocommerce/woocommerce-smallscreen.css" type="text/css" media="only screen and (max-width: 768px)" />
    <link rel="stylesheet" href="css/woocommerce/woocommerce.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/woo-style.css" type="text/css" media="all" />

    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/shortcodes.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/core.animation.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/tribe-style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/skins/skin.css" type="text/css" media="all" />

	<link rel="stylesheet" href="css/core.portfolio.css" type="text/css" media="all" />
    <link rel="stylesheet" href="js/mediaelement/mediaelementplayer.min.css" type="text/css" media="all" />
    <link rel="stylesheet" href="js/mediaelement/wp-mediaelement.css" type="text/css" media="all" />
    <link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="all" />	
    <link rel="stylesheet" href="js/core.customizer/front.customizer.css" type="text/css" media="all" />
	<link rel="stylesheet" href="js/core.messages/core.messages.css" type="text/css" media="all" />
    <link rel="stylesheet" href="js/swiper/idangerous.swiper.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/custom-style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/skins/skin-responsive.css" type="text/css" media="all" />
	

</head>

<body class="archive body_style_wide body_filled article_style_boxed top_panel_style_light top_panel_opacity_solid top_panel_above menu_right sidebar_show sidebar_right woocommerce woocommerce-page">
    <a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-angle-double-up" data-url="" data-separator="yes"></a>
	<!-- Body -->
    <div class="body_wrap">
        <div class="page_wrap">
            <div class="top_panel_fixed_wrap"></div>
            <header class="top_panel_wrap bg_tint_light">
				<!-- User menu -->
                <div class="menu_user_wrap">
                    <div class="content_wrap clearfix">
                        <div class="menu_user_area menu_user_right menu_user_nav_area">
                            <ul id="menu_user" class="menu_user_nav">
                                <li class="menu_user_currency">
                                    <a href="#">$</a>
                                    <ul>
                                        <li><a href="#"><b>&#36;</b> Dollar</a></li>
                                        <li><a href="#"><b>&euro;</b> Euro</a></li>
                                        <li><a href="#"><b>&pound;</b> Pounds</a></li>
                                    </ul>
                                </li>
                                <li class="menu_user_cart">
                                    <a href="#" class="cart_button"><span>Cart</span> <b class="cart_total"><span class="amount">&pound;511.39</span></b></a>
									<ul class="widget_area sidebar_cart sidebar fadeOutDown animated fast">
										<li class="">
											<div class="widget woocommerce widget_shopping_cart">
												<div class="hide_cart_widget_if_empty">
													<div class="widget_shopping_cart_content">
														<ul class="cart_list product_list_widget ">
															<li class="mini_cart_item">
																<a title="Remove this item" class="remove" href="#">×</a>													
																<a href="product-page.html">
																	<img alt="" src="images/masonry_03-250x250.jpg">Introduction to Biomedical Imaging&nbsp;
																</a>
																<span class="quantity">1 × <span class="amount">£350.00</span></span>					
															</li>
															<li class="mini_cart_item">
																<a title="Remove this item" class="remove" href="#">×</a>													
																<a href="product-page.html">
																	<img alt="" src="images/post_video-250x250.jpg">Introduction to Computer Science&nbsp;
																</a>
																<span class="quantity">1 × <span class="amount">£120.00</span></span>					
															</li>
															<li class="mini_cart_item">
																<a title="Remove this item" class="remove" href="#">×</a>													
																<a href="product-page.html">
																	<img alt="" src="images/image3xxl-6-250x250.jpg">Star Print Backpack&nbsp;							
																</a>
																<span class="quantity">1 × <span class="amount">£41.39</span></span>					
															</li>						
														</ul>
														<p class="total"><strong>Subtotal:</strong> <span class="amount">£511.39</span></p>
														<p class="buttons">
															<a class="button wc-forward" href="cart.html">View Cart</a>
															<a class="button checkout wc-forward" href="checkout.html">Checkout</a>
														</p>
													</div>
												</div>
											</div>
										</li>
									</ul>
                                </li>	
                                <li class="menu_user_bookmarks">
                                    <a href="#" class="bookmarks_show icon-star-1" title="Show bookmarks"></a>
                                    <ul class="bookmarks_list">
                                        <li><a href="#" class="bookmarks_add icon-star-empty" title="Add the current page into bookmarks">Add bookmark</a></li>
                                    </ul>
                                </li>
                                <li class="menu_user_controls">
                                    <a href="#">
										<span class="user_avatar">
											<img alt="" src="http://1.gravatar.com/avatar/45e4d63993e55fa97a27d49164bce80f?s=16&amp;d=mm&amp;r=g" srcset="http://1.gravatar.com/avatar/45e4d63993e55fa97a27d49164bce80f?s=32&amp;d=mm&amp;r=g 2x" class="avatar avatar-16 photo" height="16" width="16" />
										</span>
										<span class="user_name">John Doe</span></a>
                                    <ul>
                                        <li><a href="#" class="icon icon-doc-inv">New post</a></li>
                                        <li><a href="#" class="icon icon-cog-1">Settings</a></li>
                                    </ul>
                                </li>
                                <li class="menu_user_logout">
									<a href="#" class="icon icon-logout">Logout</a>
								</li>
                            </ul>
                        </div>
                        <div class="menu_user_area menu_user_left menu_user_contact_area">Contact us on 0 800 123-4567 or <a href="#"><span class="__cf_email__" data-cfemail="dfacaaafafb0adab9fabb7bab2baadbaa7f1b1baab">[email&#160;protected]</span><script data-cfhash='f9e31' type="text/javascript">
/* <![CDATA[ */!function(){try{var t="currentScript"in document?document.currentScript:function(){for(var t=document.getElementsByTagName("script"),e=t.length;e--;)if(t[e].getAttribute("data-cfhash"))return t[e]}();if(t&&t.previousSibling){var e,r,n,i,c=t.previousSibling,a=c.getAttribute("data-cfemail");if(a){for(e="",r=parseInt(a.substr(0,2),16),n=2;a.length-n;n+=2)i=parseInt(a.substr(n,2),16)^r,e+=String.fromCharCode(i);e=document.createTextNode(e),c.parentNode.replaceChild(e,c)}t.parentNode.removeChild(t);}}catch(u){}}()/* ]]> */</script></a></div>
                    </div>
                </div>
				<!-- /User menu -->
				<!-- Main menu -->
                <div class="menu_main_wrap logo_left">					
                    <div class="content_wrap clearfix">
						<!-- Logo -->
                        <div class="logo">
                            <a href="index.html">
								<img src="images/logo_light.png" class="logo_main" alt="">
								<img src="images/logo_light.png" class="logo_fixed" alt="">
							</a>
                        </div>
						<!-- Logo -->
						<!-- Search -->
                        <div class="search_wrap search_style_regular search_ajax" title="Open/close search form">
                            <a href="#" class="search_icon icon-search-2"></a>
                            <div class="search_form_wrap">
                                <form method="get" class="search_form" action="#">
                                    <button type="submit" class="search_submit icon-zoom-1" title="Start search"></button>
                                    <input type="text" class="search_field" placeholder="" value="" name="s" title="" />
                                </form>
                            </div>
                            <div class="search_results widget_area bg_tint_light">
                                <a class="search_results_close icon-delete-2"></a>
                                <div class="search_results_content">
							</div>
                            </div>
                        </div>
						<!-- /Search -->
						<!-- Navigation -->
                        <a href="#" class="menu_main_responsive_button icon-menu-1"></a>
						<nav class="menu_main_nav_area">
							<ul id="menu_main" class="menu_main_nav">
								<li class="menu-item menu-item-has-children"><a href="index.html">Homepage</a>
									<ul class="sub-menu">
										<li class="menu-item"><a href="index.html">Homepage Wide</a></li>
										<li class="menu-item"><a href="homepage-2.html">Homepage Boxed</a></li>
										<li class="menu-item"><a href="homepage-3.html">Homepage Photos</a></li>
									</ul>
								</li>
								<li class="menu-item menu-item-has-children"><a href="#">Features</a>
									<ul class="sub-menu">
										<li class="menu-item "><a href="typography.html">Typography</a></li>
										<li class="menu-item"><a href="shortcodes.html">Shortcodes</a></li>
										<li class="menu-item"><a href="video-tutorials.html">Video Tutorials</a></li>
										<li class="menu-item"><a href="events.html">Events Calendar</a></li>
										<li class="menu-item"><a href="about-us.html">About Us</a></li>
										<li class="menu-item"><a href="contact-us.html">Contact Us</a></li>
										<li class="menu-item"><a href="not-existing-page.html">Page 404</a></li>
										<li class="menu-item"><a href="not-existing-page-2.html">Page 404 (Style 2)</a></li>
									</ul>
								</li>
								<li class="menu-item menu-item-has-children"><a href="courses-streampage.html">Courses</a>
									<ul class="sub-menu">
										<li class="menu-item"><a href="courses-streampage.html">All courses</a></li>
										<li class="menu-item"><a href="free-course.html">Free course</a></li>
										<li class="menu-item"><a href="paid-course.html">Paid course</a></li>
										<li class="menu-item menu-item-has-children"><a href="#">Lessons</a>
											<ul class="sub-menu">
												<li class="menu-item"><a href="free-lesson.html">Free lesson (started)</a></li>
												<li class="menu-item"><a href="free-lesson-coming-soon.html">Free lesson (coming soon)</a></li>
												<li class="menu-item"><a href="lesson-from-paid-course.html">Lesson from paid course</a></li>
											</ul>
										</li>
									</ul>
								</li>
								<li class="menu-item menu-item-has-children"><a href="team-members.html">Teachers</a>
									<ul class="sub-menu">
										<li class="menu-item"><a href="team-members.html">Teachers Team</a></li>
										<li class="menu-item"><a href="personal-page.html">Teacher&#8217;s Personal Page</a></li>
									</ul>
								</li>
								<li class="menu-item menu-item-has-children"><a href="blog-streampage.html">Blog</a>
									<ul class="sub-menu">
										<li class="menu-item menu-item-has-children"><a href="#">Post Formats</a>
											<ul class="sub-menu">
												<li class="menu-item"><a href="post-formats-with-sidebar.html">With Sidebar</a></li>
												<li class="menu-item"><a href="post-formats.html">Without sidebar</a></li>
											</ul>
										</li>
										<li class="menu-item menu-item-has-children"><a href="#">Masonry tiles</a>
											<ul class="sub-menu">
												<li class="menu-item"><a href="masonry-2-columns.html">Masonry (2 columns)</a></li>
												<li class="menu-item"><a href="masonry-3-columns.html">Masonry (3 columns)</a></li>
											</ul>
										</li>
										<li class="menu-item menu-item-has-children"><a href="#">Portfolio tiles</a>
											<ul class="sub-menu">
												<li class="menu-item"><a href="portfolio-2-columns.html">Portfolio (2 columns)</a></li>
												<li class="menu-item"><a href="portfolio-3-columns.html">Portfolio (3 columns)</a></li>
												<li class="menu-item menu-item-has-children"><a href="#">Portfolio hovers</a>
													<ul class="sub-menu">
														<li class="menu-item"><a href="portfolio-hovers-circle.html">Circle, Part 1</a></li>
														<li class="menu-item"><a href="portfolio-hovers-circle-part-2.html">Circle, Part 2</a></li>
														<li class="menu-item"><a href="portfolio-hovers-circle-part-3.html">Circle, Part 3</a></li>
														<li class="menu-item"><a href="portfolio-hovers-square.html">Square, Part 1</a></li>
														<li class="menu-item"><a href="portfolio-hovers-square-part-2.html">Square, Part 2</a></li>
														<li class="menu-item"><a href="portfolio-hovers-square-part-3.html">Square, Part 3</a></li>
													</ul>
												</li>
											</ul>
										</li>
									</ul>
								</li>
								<li class="menu-item current-menu-item page_item current_page_item"><a href="products.html">Shop</a></li>
							</ul>
						</nav>
						<!-- /Navigation -->
                    </div>
                </div>
				<!-- /Main menu -->
            </header>
			<!-- Page title -->
            <div class="page_top_wrap page_top_title page_top_breadcrumbs sc_pt_st1">
                <div class="content_wrap">
                    <div class="breadcrumbs">
                        <a class="breadcrumbs_item home" href="index.html">Home</a>
						<span class="breadcrumbs_delimiter"></span>
						<span class="breadcrumbs_item current">Shop</span>
					</div>
                    <h1 class="page_title">Shop</h1>
                </div>
            </div>
			<!-- /Page title -->
			<!-- Content with sidebar -->
            <div class="page_content_wrap">
                <div class="content_wrap">
					<!-- Content -->
                    <div class="content">
                        <div class="list_products shop_mode_thumbs">
                            <form class="woocommerce-ordering" method="get">
                                <select name="orderby" class="orderby">
                                    <option value="menu_order" selected="selected">Default sorting</option>
                                    <option value="popularity">Sort by popularity</option>
                                    <option value="rating">Sort by average rating</option>
                                    <option value="date">Sort by newness</option>
                                    <option value="price">Sort by price: low to high</option>
                                    <option value="price-desc">Sort by price: high to low</option>
                                </select>
                            </form>
                            <ul class="products">
                                <li class="first product has-post-thumbnail column-1_2">
                                    <a href="product-page.html"></a>
									<div class="post_item_wrap">
										<div class="post_featured">
											<div class="post_thumb">
												<a class="hover_icon hover_icon_link" href="product-page.html">
													<img src="images/71Bs52AaA7L-400x400.jpg" alt="" /> 
												</a>
											</div>
										</div>
										<div class="post_content">
											<h3><a href="product-page.html">200 jazz licks</a></h3>
											<span class="price">
												<span class="amount">&pound;25.69</span>
											</span>
											<a href="#" class="button add_to_cart_button ">Add to cart</a> 
										</div>
                                    </div>
                                </li>
                                <li class="last product has-post-thumbnail column-1_2">
                                    <a href="product-page.html"></a>
									<div class="post_item_wrap">
										<div class="post_featured">
											<div class="post_thumb">
												<a class="hover_icon hover_icon_link" href="product-page.html">
													<img src="images/515s1QORy4L-400x400.jpg" alt="" /> </a>
											</div>
										</div>
										<div class="post_content">
											<h3><a href="product-page.html">Adobe Photoshop Lightroom 5</a></h3>
											<span class="price">
												<span class="amount">&pound;374.19</span>
											</span>
											<a href="#" class="button add_to_cart_button">Add to cart</a> 
										</div>
                                    </div>
                                </li>
                                <li class="first product has-post-thumbnail column-1_2 sale">
                                    <a href="product-page.html"></a>
									<div class="post_item_wrap">
										<div class="post_featured">
											<div class="post_thumb">
												<a class="hover_icon hover_icon_link" href="product-page.html">
													<span class="onsale">Sale!</span>
													<img src="images/masonry_06-400x400.jpg" alt="" /> </a>
											</div>
										</div>
										<div class="post_content">
											<h3><a href="product-page.html">Entrepreneurship 101: Who is your customer?</a></h3>
											<span class="price">
												<del><span class="amount">&pound;195.00</span></del> 
												<ins><span class="amount">&pound;180.00</span></ins>
											</span>
											<a href="#" class="button add_to_cart_button">Add to cart</a> 
										</div>
									</div>
                                </li>
                                <li class="last product type-product has-post-thumbnail column-1_2">
                                    <a href="product-page.html"></a>
									<div class="post_item_wrap">
										<div class="post_featured">
											<div class="post_thumb">
												<a class="hover_icon hover_icon_link" href="product-page.html">
													<img src="images/8178IYIHlUL._SL1500_-400x400.jpg" alt="" /> 
												</a>
											</div>
										</div>
										<div class="post_content">
											<h3><a href="product-page.html">Geology Fact Book</a></h3>
											<span class="price">
												<span class="amount">&pound;56.69</span>
											</span>				
											<a href="#" class="button add_to_cart_button">Add to cart</a> 
										</div>
									</div>
                                </li>
                                <li class="first product has-post-thumbnail column-1_2 sale">
                                    <a href="product-page.html"></a>
									<div class="post_item_wrap">
										<div class="post_featured">
											<div class="post_thumb">
												<a class="hover_icon hover_icon_link" href="product-page.html">
													<span class="onsale">Sale!</span>
													<img src="images/masonry_03-400x400.jpg" alt="" /> 
												</a>
											</div>
										</div>
										<div class="post_content">
											<h3><a href="product-page.html">Introduction to Biomedical Imaging</a></h3>
											<span class="price">
												<del><span class="amount">&pound;400.00</span></del> 
												<ins><span class="amount">&pound;350.00</span></ins>
											</span>
											<a href="#" class="button add_to_cart_button">Add to cart</a> 
										</div>
									</div>
                                </li>
                                <li class="last product has-post-thumbnail column-1_2">
                                    <a href="product-page.html"></a>
									<div class="post_item_wrap">
										<div class="post_featured">
											<div class="post_thumb">
												<a class="hover_icon hover_icon_link" href="product-page.html">
													<img src="images/post_video-400x400.jpg" alt="" /> 
												</a>
											</div>
										</div>
										<div class="post_content">
											<h3><a href="product-page.html">Introduction to Computer Science</a></h3>
											<span class="price">
												<span class="amount">&pound;120.00</span>
											</span>
											<a href="#" class="button add_to_cart_button product_type_simple">Add to cart</a> 
										</div>
                                    </div>
                                </li>
                                <li class="first product thumbnail column-1_2">
                                    <a href="product-page.html"></a>
									<div class="post_item_wrap">
										<div class="post_featured">
											<div class="post_thumb">
												<a class="hover_icon hover_icon_link" href="product-page.html">
													<img src="images/image1xxl3-400x400.jpg" alt="" /> 
												</a>
											</div>
										</div>
										<div class="post_content">
											<h3><a href="product-page.html">Leather Cap</a></h3>
											<span class="price">
												<span class="amount">&pound;20.00</span>
											</span>
											<a href="#" class="button add_to_cart_button">Add to cart</a> 
										</div>
                                    </div>
                                </li>
                                <li class="last product has-post-thumbnail column-1_2">
                                    <a href="product-page.html"></a>
									<div class="post_item_wrap">
										<div class="post_featured">
											<div class="post_thumb">
												<a class="hover_icon hover_icon_link" href="product-page.html">
													<img src="images/image4xxl-2-400x400.jpg" alt="" /> 
												</a>
											</div>
										</div>
										<div class="post_content">
											<h3><a href="product-page.html">Long Sleeve Top With  Raglan Sleeve</a></h3>
											<span class="price">
												<span class="amount">&pound;33.25</span>
											</span>
											<a href="#" class="button add_to_cart_button">Add to cart</a> 
										</div>
                                    </div>
                                </li>
                                <li class="first product has-post-thumbnail column-1_2">
                                    <a href="product-page.html"></a>
									<div class="post_item_wrap">
										<div class="post_featured">
											<div class="post_thumb">
												<a class="hover_icon hover_icon_link" href="product-page.html">
													<img src="images/masonry_01-400x400.jpg" alt="" /> 
												</a>
											</div>
										</div>
										<div class="post_content">
											<h3><a href="product-page.html">Medical Chemistry: The Molecular Basis…</a></h3>
											<span class="price">
												<span class="amount">&pound;185.00</span>
											</span>
											<a href="#" class="button add_to_cart_button">Add to cart</a> 
										</div>
                                    </div>
                                </li>
                                <li class="last product has-post-thumbnail column-1_2">
                                    <a href="product-page.html"></a>
									<div class="post_item_wrap">
										<div class="post_featured">
											<div class="post_thumb">
												<a class="hover_icon hover_icon_link" href="product-page.html">
													<img src="images/masonry_05-400x400.jpg" alt="" /> 
												</a>
											</div>
										</div>
										<div class="post_content">
											<h3><a href="product-page.html">Principles of Written English, Part 1</a></h3>
											<span class="price">
												<span class="amount">&pound;85.00</span>
											</span>                                
											<a href="#" class="button add_to_cart_button">Add to cart</a> 
										</div>
                                    </div>
                                </li>
                                <li class="first product has-post-thumbnail column-1_2">
                                    <a href="product-page.html"></a>
									<div class="post_item_wrap">
										<div class="post_featured">
											<div class="post_thumb">
												<a class="hover_icon hover_icon_link" href="product-page.html">
													<img src="images/masonry_15-400x400.jpg" alt="" /> 
												</a>
											</div>
										</div>
										<div class="post_content">
											<h3><a href="product-page.html">Principles of Written English, Part 2</a></h3>
											<div class="star-rating" title="Rated 5.00 out of 5">
												<span class="width_100per"><strong class="rating">5.00</strong> out of 5</span>
											</div>
											<span class="price">
												<span class="amount">&pound;85.00</span>
											</span>
											<a href="#" class="button add_to_cart_button">Add to cart</a> 
										</div>
                                    </div>
                                </li>
                                <li class="last product has-post-thumbnail column-1_2">
                                    <a href="product-page.html"></a>
									<div class="post_item_wrap">
										<div class="post_featured">
											<div class="post_thumb">
												<a class="hover_icon hover_icon_link" href="product-page.html">
													<img src="images/image3xxl-6-400x400.jpg" alt="" /> 
												</a>
											</div>
										</div>
										<div class="post_content">
											<h3><a href="product-page.html">Star Print Backpack</a></h3>
											<div class="star-rating" title="Rated 5.00 out of 5">
												<span class="width_100per"><strong class="rating">5.00</strong> out of 5</span>
											</div>
											<span class="price">
												<span class="amount">&pound;41.39</span>
											</span>                                   
											<a href="#" class="button add_to_cart_button product_type_simple">Add to cart</a> 
										</div>
                                    </div>
                                </li>
                            </ul>
                            <nav id="pagination" class="pagination_wrap pagination_pages">
								<span class="pager_current active ">1</span>
								<a href="#" class="">2</a>
                                <a href="#" class="pager_next "></a>
                                <a href="#" class="pager_last "></a>
                            </nav>
                        </div>
                    </div>
					<!-- /Content -->
                    <!-- Sidebar -->
                    <div class="sidebar widget_area bg_tint_light sidebar_style_light">
						<!-- Product category widget -->
                        <aside class="woocommerce">
                            <h5 class="widget_title">Product Categories</h5>
                            <ul class="product-categories">
                                <li><a href="product-category.html">Computer</a>
									<span class="count">(1)</span>
								</li>
                                <li>
									<a href="product-category.html">Courses</a> 
									<span class="count">(0)</span>
								</li>
                                <li>
									<a href="product-category.html">Language</a> 
									<span class="count">(2)</span>
								</li>
                                <li>
									<a href="product-category.html">Marketing</a> 
									<span class="count">(2)</span>
								</li>
                                <li>
									<a href="product-category.html">Medicine</a> 
									<span class="count">(2)</span>
								</li>
                                <li>
									<a href="product-category.html">Products</a> 
									<span class="count">(8)</span>
								</li>
                            </ul>
                        </aside>
						<!-- /Product category widget -->
						<!-- Recent products widget -->
                        <aside class="woocommerce">
                            <h5 class="widget_title">Recent Products</h5>
                            <ul class="product_list_widget">
                                <li>
                                    <a href="product-page.html" title="">
                                        <img src="images/image1xxl3-250x250.jpg" alt="" /> 
										<span class="product-title">Leather Cap</span>
                                    </a>
                                    <span class="amount">&pound;20.00</span></li>
                                <li>
                                    <a href="product-page.html" title="">
                                        <img src="images/image3xxl-6-250x250.jpg" alt="" /> 
										<span class="product-title">Star Print Backpack</span>
                                    </a>
                                    <span class="amount">&pound;41.39</span>
								</li>
                                <li>
                                    <a href="product-page.html" title="Long Sleeve Top With  Raglan Sleeve">
                                        <img src="images/image4xxl-2-250x250.jpg" alt="" /> 
										<span class="product-title">Long Sleeve Top With  Raglan Sleeve</span>
                                    </a>
                                    <span class="amount">&pound;33.25</span>
								</li>
                                <li>
                                    <a href="#" title="Yellow Backpack">
                                        <img src="images/image1xxl-33-250x250.jpg" alt="" /> 
										<span class="product-title">Yellow Backpack</span>
                                    </a>
                                    <span class="amount">&pound;74.19</span>
								</li>
                            </ul>
                        </aside>
						<!-- /Recent products widget -->
						<!-- Product tags widget -->
                        <aside class="woocommerce widget_product_tag_cloud">
                            <h5 class="widget_title">Product Tags</h5>
                            <div class="tagcloud">
								<a href="product-tag.html" title="2 topics">backpacks</a>
                                <a href="product-tag.html" title="3 topics">books</a>
                                <a href="product-tag.html" title="1 topic">caps</a>
                                <a href="product-tag.html" title="1 topic">clothes</a>
                                <a href="product-tag.html" title="1 topic">computer</a>
                                <a href="product-tag.html" title="6 topics">courses</a>
                                <a href="product-tag.html" title="1 topic">design</a>
                                <a href="product-tag.html" title="1 topic">geology</a>
                                <a href="product-tag.html" title="2 topics">language</a>
                                <a href="product-tag.html" title="1 topic">maps</a>
                                <a href="product-tag.html" title="2 topics">marketing</a>
                                <a href="product-tag.html" title="2 topics">medicine</a>
                                <a href="product-tag.html" title="1 topic">music</a>
                                <a href="product-tag.html" title="2 topics">seo</a>
                                <a href="product-tag.html" title="1 topic">soft</a></div>
                        </aside>
						<!-- /Product tags widget -->
						<!-- Top rated products widget -->
                        <aside class="woocommerce">
                            <h5 class="widget_title">Top Rated Products</h5>
                            <ul class="product_list_widget">
                                <li>
                                    <a href="product-page.html" title="Star Print Backpack">
                                        <img src="images/image3xxl-6-250x250.jpg" alt="" /> 
										<span class="product-title">Star Print Backpack</span>
                                    </a>
                                    <div class="star-rating" title="Rated 5.00 out of 5">
										<span class="width_100per">
											<strong class="rating">5.00</strong> out of 5
										</span>
									</div> 
									<span class="amount">&pound;41.39</span>
								</li>
                                <li>
                                    <a href="product-page.html" title="Principles of Written English, Part 2">
                                        <img src="images/masonry_15-250x250.jpg" alt="" /> 
										<span class="product-title">Principles of Written English, Part 2</span>
                                    </a>
                                    <div class="star-rating" title="Rated 5.00 out of 5">
										<span class="width_100per">
											<strong class="rating">5.00</strong> out of 5
										</span>
									</div> 
									<span class="amount">&pound;85.00</span>
								</li>
                                <li>
                                    <a href="product-page.html" title="Video Training for Microsoft products and technologies">
                                        <img src="images/masonry_13-250x250.jpg" alt="" /> 
										<span class="product-title">Video Training for Microsoft products and technologies</span>
                                    </a>
                                    <div class="star-rating" title="Rated 5.00 out of 5">
										<span class="width_100per">
											<strong class="rating">5.00</strong> out of 5
										</span>
									</div> 
									<span class="amount">&pound;150.00</span>
								</li>
                                <li>
                                    <a href="product-page.html" title="Yellow Backpack">
                                        <img src="images/image1xxl-33-250x250.jpg" alt="" /> 
										<span class="product-title">Yellow Backpack</span>
                                    </a>
                                    <div class="star-rating" title="Rated 4.00 out of 5">
										<span class="width_80per">
											<strong class="rating">4.00</strong> out of 5
										</span>
									</div> 
								<span class="amount">&pound;74.19</span></li>
                            </ul>
                        </aside>
						<!-- /Top rated products widget -->
                    </div>
                    <!-- /Sidebar -->
                </div>
            </div>
            <!-- /Content -->		
				<?php include 'footer.php';?>